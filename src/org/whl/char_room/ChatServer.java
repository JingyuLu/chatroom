package org.whl.char_room;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {
	private List<ClientChannel>	clientList=new ArrayList<ClientChannel>();

	public static void main(String[] args) throws IOException {
		new ChatServer().start();
	}

	public void start() throws IOException {
		ServerSocket server = new ServerSocket(9999);

		while (true) {
			Socket client = server.accept();

			ClientChannel channel = new ClientChannel(client);

			clientList.add(channel);

			new Thread(channel).start();
		}
	}

	class ClientChannel implements Runnable {
		DataInputStream dis;
		DataOutputStream dos;
		private boolean isRunning = true;
		private String name;

		public ClientChannel(Socket client) {
			try {
				dis = new DataInputStream(client.getInputStream());
				dos = new DataOutputStream(client.getOutputStream());
			
				this.name=dis.readUTF();

				this.send("Welcome!");
				sendAll(this.name+"has already joined",true);
			} catch (IOException e) {
				// e.printStackTrace();
				CloseStream.closeAll(dis, dos);
				isRunning = false;
			}
		}

		private String receive() {
			String msg = "";
			try {
				msg = dis.readUTF();
			} catch (IOException e) {
				CloseStream.closeAll(dis);
				isRunning = false;
				clientList.remove(this);
			}
			return msg;
		}

		private void send(String msg) {
			if (msg == null || msg.equals("")) {
				return;
			}
			try {
				dos.writeUTF(msg);
				dos.flush();
			} catch (IOException e) {
				CloseStream.closeAll(dos);
				isRunning = false;
				clientList.remove(this);
			}
		}
		
		private void sendAll(String msg,boolean sys) {

			if(msg.startsWith("@")&&msg.indexOf(":")>-1) {
				String name=msg.substring(1,msg.indexOf(":"));
				String content=msg.substring(msg.indexOf(":")+1);
				for(ClientChannel allClients:clientList) {
					if(allClients.equals(name)) {
						allClients.send(this.name+"tell you secretly:"+content);
					}
				}
			}else {
				for(ClientChannel allClients:clientList) {
					if(allClients==this) {
						continue;
					}
					if(sys==true) {
						allClients.send("System:"+msg);
					}else {
						allClients.send(this.name+"to all:"+msg);
					}
					
				}
			}
		}

		@Override
		public void run() {
			while (isRunning) {
				sendAll(receive(),false);
			}
		}

	}
}
